Бот-помошник в Телеграм.
Бот может предоставить контакты преподавателей позволит узнать актуальное расписание группы. Также чат-бот предоставит ряд ссылок на полезные ресурсы, в том числе и на необходимые для учебы программы и литературу. Помимо всего добавлена опция с частыми вопросами для студентов и интересными фактами связанными со специальностью. 

Bot assistant in the Telegram.
The bot can provide contacts of teachers and will allow you to find out the current schedule of the group. Also, the chatbot will provide a number of links to useful resources, including the necessary programs and literature for studying. In addition, an option with frequent questions for students and interesting facts related to the specialty has been added.