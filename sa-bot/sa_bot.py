#файлы
import config
import sprav
import links
import prog
import raspis
import cool
#библиотеки
import random
import telebot

#создал объект-бот
bot = telebot.TeleBot(config.token)

#стартовые кнопки
kb_start = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True)
kb_start.add('Справочник', 'Ссылки','Программы', 'Расписание', 'Интересное')

#обработка команд
@bot.message_handler(commands=['start'])
def start_message(message):
    bot.send_message(message.chat.id, 'Привет, я бот-помощник!\nЧтобы узнать возможности отправьте /help', reply_markup = kb_start)

@bot.message_handler(commands=['help'])
def help_message(message):
    bot.send_message(message.chat.id, 'Справочник - контакты преподавателей. Ссылки - полезные ссылки на различные ресурсы.')

@bot.message_handler(commands=['stop'])
def stop_message(message):
    bot.send_message(message.chat.id, 'ok', reply_markup=None)

#обработка сообщений
@bot.message_handler(content_types=['text'])
def main_menu(message):
    if message.text == 'Справочник':
        kb_sprav = telebot.types.InlineKeyboardMarkup(row_width=1)
        for key in sprav.spr:
            cur_but = telebot.types.InlineKeyboardButton(text=key, callback_data=key)
            kb_sprav.add(cur_but)
        bot.send_message(message.chat.id, 'Справочник:', reply_markup=kb_sprav)
    
    elif message.text == 'Ссылки':
        kb_links = telebot.types.InlineKeyboardMarkup()
        for key in links.ls:
            cur_but = telebot.types.InlineKeyboardButton(text=key, url=links.ls[key], callback_data="linked")
            kb_links.add(cur_but)
        bot.send_message(message.chat.id, 'Ссылки:', reply_markup=kb_links)
       
    elif message.text == 'Программы':
        kb_prog = telebot.types.InlineKeyboardMarkup()
        for key in prog.pr:
             cur_but = telebot.types.InlineKeyboardButton(text=key, url=prog.pr[key], callback_data="pr")
             kb_prog.add(cur_but)
        bot.send_message(message.chat.id, 'Программы:', reply_markup=kb_prog)
    
    elif message.text == 'Расписание':
        kb_raspis = telebot.types.InlineKeyboardMarkup()
        for key in raspis.ras:
             cur_but = telebot.types.InlineKeyboardButton(text=key, url=raspis.ras[key], callback_data="ras")
             kb_raspis.add(cur_but)
        bot.send_message(message.chat.id, 'Выбор группы:', reply_markup=kb_raspis)
    
    elif message.text == 'Интересное':
        i = str(random.randint(1, 10))
        bot.send_message(message.chat.id, cool.cool[i])
            

@bot.callback_query_handler(func=lambda call: True)
def callback_inline(call):
    if call.message.text == "Справочник:":
        bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.id, text=call.data + '\n' + sprav.spr[call.data], reply_markup=None)
    elif call.data == "linked":
        bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.id, text="Ссылки...", reply_markup=None)


#циклическая проверка на новые сообщения боту
bot.polling()

